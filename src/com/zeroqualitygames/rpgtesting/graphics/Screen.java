package com.zeroqualitygames.rpgtesting.graphics;

import java.util.Random;

import com.zeroqualitygames.rpgtesting.graphics.Color;
import com.zeroqualitygames.rpgtesting.graphics.TileSet;

public class Screen {
	private Random random = new Random();
	
	public TileSet image;
	public int tileWidth = 30;
	public int tileHeight = 6;
	public int tileSize = 16;
	public int width;
	public int height;
	public int[] pixels;
	
	public Screen(int width, int height, TileSet image) {
		this.width = width;
		this.height = height;
		this.image = image;
		pixels = new int[width * height];
	}
	
	public void randomise() {
		for (int i = 0; i < pixels.length; i++) {
			int pixel = random.nextInt();
			pixels[i] = pixel;
			pixels[i] = 0;
		}
	}
	
	public void render(int xOffs, int yOffs, int tile, int color) {
		
		int tileSize = 8;
		int xTile = tile % tileWidth;
		int yTile = tile / tileWidth;
		for (int y = 0; y < tileSize; y++ ) {
			if (y + yOffs < 0 || y + yOffs >= height) continue;
			
			for (int x = 0; x < tileSize; x++) {
				if (x + xOffs < 0 || x + xOffs >= width) continue;
				
				int xt = xTile * tileSize + x;
				int yt = yTile * tileSize + y;
				int pixel = Color.get(image.pixels[xt + yt * image.width]);
				if (pixel == Color.get(64, 128, 128)) continue;
				pixels[(xOffs + x) + (yOffs + y) * width] = pixel;
			}
		}
	}
	
	public String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ " +
							   "0123456789.,!?'\"-+=/\\%()<>:;";
	
	public void drawString(String str, int xOffs, int yOffs) {
		str = str.toUpperCase();
		for (int i = 0; i < str.length(); i++) {
			int location = characters.indexOf(str.charAt(i));
			if (location >= characters.indexOf('0'))
				location += 3;
			int tile =  location + 14 * 30;
			render(xOffs + (i * 8), yOffs, tile, Color.get(155, 155, 155));
		}
	}
	
	public void renderTextBox(int xTile, int yTile, int xOffsTile, int yOffsTile) {
		yOffsTile += yTile;
		xOffsTile += xTile;
		
		for (int y = yTile; y <= yOffsTile; y++) {
			for (int x = xTile; x <= xOffsTile; x++) {
				if (y == yTile && x == xTile)
					render(x * 8, y * 8, 0 + 10 * 30, 0);
				else if (y == yTile && x == xOffsTile)
					render(x * 8, y * 8, 2 + 10 * 30, 0);
				else if (y == yOffsTile && x == xTile)
					render(x * 8, y * 8, 0 + 12 * 30, 0);
				else if (y == yOffsTile && x == xOffsTile)
					render(x * 8, y * 8, 2 + 12 * 30, 0);
				else if (x == xTile)
					render(x * 8, y * 8, 0 + 11 * 30, 0);
				else if (x == xOffsTile)
					render(x * 8, y * 8, 2 + 11 * 30, 0);
				else if (y == yTile)
					render(x * 8, y * 8, 1 + 10 * 30, 0);
				else if (y == yOffsTile)
					render(x * 8, y * 8, 1 + 12 * 30, 0);
				else
					render(x * 8, y * 8, 1 + 11 * 30, 0);
			}
		}
	}

}
