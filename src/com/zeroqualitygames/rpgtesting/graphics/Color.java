package com.zeroqualitygames.rpgtesting.graphics;

public class Color {

	public Color() {
		// TODO Auto-generated constructor stub
	}
	
	public static int get(int r, int g, int b) {
		return r << 16 | g << 8 | b;
	}
	
	public static int get(int a) {
		int r = a >> 16 & 0x000000FF;
		int g = a >> 8 & 0x000000FF;
		int b = a & 0x000000FF;
		return r << 16 | g << 8 | b;
	}

}
