package com.zeroqualitygames.rpgtesting.graphics;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

public class TileSet {
	public BufferedImage image;
	public int width;
	public int height;
	public int[] pixels;

	public TileSet(String name) {
		URL url = this.getClass().getResource(name);
		try {
			image = ImageIO.read(url);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		width = image.getWidth();
		height = image.getHeight();
		
		pixels = image.getRGB(0, 0, width, height, null, 0, width);
	}

}
