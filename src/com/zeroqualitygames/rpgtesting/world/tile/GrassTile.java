package com.zeroqualitygames.rpgtesting.world.tile;

import java.awt.Rectangle;

import com.zeroqualitygames.rpgtesting.graphics.Screen;

public class GrassTile extends Tile {
	public int tile = 0 + 0 * 30;

	public GrassTile(String ID, int x, int y) {
		super(ID, x, y);
		rect = new Rectangle(x, y, 16, 16);
	}
	
	public void render(Screen screen) {
		super.render(screen);
	}
	
	public void remove() {
		super.remove();
	}
}
