package com.zeroqualitygames.rpgtesting.world.tile;

import java.awt.Rectangle;

import com.zeroqualitygames.rpgtesting.graphics.Screen;

public class SignTile extends Tile {
	
	public int tile = (14 + random.nextInt(3)*2) + 2 * 30;
	public int grassTile = 0 + 0 * 30;
	@SuppressWarnings("unused")
	private Rectangle read;

	public SignTile(String ID, int x, int y) {
		super(ID, x, y);
		rect = new Rectangle(x, y, 16, 16);
		read = new Rectangle(x, y - 16, 16, 16);
	}
	
	public void tick() {
		
	}
	
	public void render(Screen screen) {
		super.render(screen);
		screen.render(x		, y		, tile		, 0);
		screen.render(x + 8	, y		, tile + 1	, 0);
		screen.render(x		, y + 8	, tile + 30	, 0);
		screen.render(x + 8	, y + 8	, tile + 31	, 0);
	}
	
	public boolean canPass() {
		return false;
	}
	
	public boolean canRead() {
		return true;
	}

	public void remove() {
		super.remove();
	}
}
