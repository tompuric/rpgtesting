package com.zeroqualitygames.rpgtesting.world.tile;

import java.awt.Rectangle;
import java.util.Random;

import com.zeroqualitygames.rpgtesting.graphics.Screen;
import com.zeroqualitygames.rpgtesting.world.World;

public class Tile {
	protected Random random = new Random();
	
	public final String ID;
	public World world;
	public int x;
	public int y;
	public Rectangle rect;
	public int grassTile = 0 + 0 * 30;
	public boolean removed = false;

	public Tile(String ID, int x, int y) {
		this.ID = ID;
		this.x = x;
		this.y = y;
	}
	
	public void init(World world) {
		this.world = world;
	}
	
	public void tick() {
		
	}

	public void render(Screen screen) {
		screen.render(x		, y		, grassTile		, 0);
		screen.render(x + 8	, y		, grassTile + 1	, 0);
		screen.render(x		, y + 8	, grassTile + 30	, 0);
		screen.render(x + 8	, y + 8	, grassTile + 31	, 0);
	}
	
	public boolean canPass() {
		return true;
	}
	
	public void remove() {
		removed = true;
	}

	public boolean canRead() {
		return false;
	}

}
