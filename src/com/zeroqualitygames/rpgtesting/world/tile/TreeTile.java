package com.zeroqualitygames.rpgtesting.world.tile;

import java.awt.Rectangle;

import com.zeroqualitygames.rpgtesting.graphics.Screen;

public class TreeTile extends Tile {
	public int tile = (6 + random.nextInt(3) * 2) + 0 * 30;
	

	public TreeTile(String ID, int x, int y) {
		super(ID, x , y);
		rect = new Rectangle(x, y, 16, 16);
	}
	
	public void render(Screen screen) {
		super.render(screen);
		screen.render(x		, y		, tile		, 0);
		screen.render(x + 8	, y		, tile + 1	, 0);
		screen.render(x		, y + 8	, tile + 30	, 0);
		screen.render(x + 8	, y + 8	, tile + 31	, 0);
	}
	
	public boolean canPass() {
		return false;
	}
	
	public void remove() {
		super.remove();
	}
}
