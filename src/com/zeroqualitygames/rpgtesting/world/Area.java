package com.zeroqualitygames.rpgtesting.world;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import com.zeroqualitygames.rpgtesting.graphics.Screen;
import com.zeroqualitygames.rpgtesting.world.tile.GrassTile;
import com.zeroqualitygames.rpgtesting.world.tile.SignTile;
import com.zeroqualitygames.rpgtesting.world.tile.Tile;
import com.zeroqualitygames.rpgtesting.world.tile.TreeTile;

public class Area {
	public ArrayList<Tile> tiles = new ArrayList<Tile>();
	private String name = "Maps/Map";
	private String extension = ".txt";
	public int x;
	public int y;
	private int size = 16;

	public Area(int y, int x) {
		this.y = y;
		this.x = x;
		name = name + y + x + extension;
		loadMap(name);
	}
	
	public void tick() {
		ArrayList<Tile> t = tiles;
		for (int i = 0; i < tiles.size(); i++) {
			if (t.get(i).removed) {
				tiles.remove(t);
			}
		}
	}
	
	public void render(Screen screen) {
		ArrayList<Tile> t = tiles;
		for (int i = 0; i < t.size(); i++) {
			t.get(i).render(screen);
		}
	}
	
	public void loadMap(String name) {
		System.out.println(name);
		InputStream stream = this.getClass().getResourceAsStream(name);
		
		try {
			System.out.println(stream.toString());
			InputStreamReader isr = new InputStreamReader(stream);
			BufferedReader reader = new BufferedReader(isr);
			int charValue;
			int x = 0;
			int y = 0;
			
			while ((charValue = reader.read()) != -1) {
				char c = (char) charValue;
				if (c == '\n' ) {
					x = 0;
					y += size;
					continue;
				}
				getTile (c, x, y);
				x += size;
			}
			

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public void getTile(char c, int x, int y) {
		
		switch (c) {
			case 'g':
				tiles.add(new GrassTile("grass" + this.x + this.y, x, y));
				break;
			case 's':
				tiles.add(new SignTile("sign" + this.x + this.y, x, y));
				break;
			case 't':
				tiles.add(new TreeTile("tree" + x + y, x, y));
				break;
			default:
				break;
		}
	}

}
