package com.zeroqualitygames.rpgtesting.world;

import java.awt.Rectangle;
import java.util.ArrayList;

import com.zeroqualitygames.rpgtesting.Game;
import com.zeroqualitygames.rpgtesting.entity.Entity;
import com.zeroqualitygames.rpgtesting.entity.Player;
import com.zeroqualitygames.rpgtesting.graphics.Screen;
import com.zeroqualitygames.rpgtesting.world.tile.Tile;

public class World {
	public Game game;
	public ArrayList<Entity> entities = new ArrayList<Entity>();
	public ArrayList<Tile> tiles = new ArrayList<Tile>();
	public Area[][] area = new Area[3][3];
	public int currentX = 1;
	public int currentY = 0;
	public Player player;
	
	public static final int RIGHT = 1;
	public static final int LEFT = -1;
	public static final int DOWN = 1;
	public static final int UP = -1;
	
	
	public World (Game game) {
		this.game = game;
		area[currentY][currentX] = new Area(currentY, currentX);
		for (Tile t : tiles) {
			t.init(this);
		}
		tiles = getArea().tiles;
		
		
	}
	
	public void init() {
		for (Entity e : entities) {
			e.init(this);
		}
	}
	
	public void tick() {
		getArea().tick();
		
		for (Entity e : entities) {
			e.tick();
		}
		
		if (player.x < 0) {
			currentX += LEFT;
			player.setX(Game.WIDTH - player.width);
			area[currentY][currentX] = new Area(currentY, currentX);
			tiles = getArea().tiles;
			return;
		}
		
		if (player.x + player.width > Game.WIDTH) {
			currentX += RIGHT;
			player.setX(0);
			area[currentY][currentX] = new Area(currentY, currentX);
			tiles = getArea().tiles;
			return;
		}
		
		if (player.y < 0) {
			currentY += UP;
			player.setY(Game.HEIGHT - player.height);
			area[currentY][currentX] = new Area(currentY, currentX);
			tiles = getArea().tiles;
			return;
		}
		
		if (player.y + player.height > Game.HEIGHT) {
			currentY += DOWN;
			player.setY(0);
			area[currentY][currentX] = new Area(currentY, currentX);
			tiles = getArea().tiles;
			return;
		}
		
		
	}
	
	public void render(Screen screen) {
		getArea().render(screen);
		player.render(screen);
	}
	
	public Area getArea() {
		return area[currentY][currentX];
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public void notifyEnter() {
		System.out.println("Hello");
	}
	
	
	public void addEntity(Entity e) {
		if (e instanceof Player) {
			player = (Player) e;
		}
		entities.add(e);
	}
	
	public ArrayList<Entity> getEntities(Rectangle rect) {
		return null;
	}
	
	public ArrayList<Tile> getTiles(Rectangle rect) {
		ArrayList<Tile> t = new ArrayList<Tile>();
		for (int i = 0; i < tiles.size(); i++) {
			if (tiles.get(i).rect.intersects(rect)) {
				t.add(tiles.get(i));
			}
		}
		return t;
	}
	
}
