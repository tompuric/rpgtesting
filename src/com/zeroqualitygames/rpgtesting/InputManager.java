package com.zeroqualitygames.rpgtesting;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

public class InputManager implements KeyListener {

	public class Key {
		public boolean pressed;
		public boolean clicked;
		private int absorbs;
		public int numPressed;
		
		public Key() {
			keys.add(this);
		}
		
		public void toggle(boolean pressed) {
			this.pressed = pressed;
			if (pressed)
				numPressed++;
		}
		
		public void tick() {
			if (absorbs < numPressed) {
				absorbs++;
				clicked = true;
			}
			else
				clicked = false;
		}
	}
	
	ArrayList<Key> keys = new ArrayList<Key>();
	
	public Key up = new Key();
	public Key down = new Key();
	public Key right = new Key();
	public Key left = new Key();
	
	public Key enter = new Key();
	public Key escape = new Key();
	
	public InputManager(Game game) {
		game.addKeyListener(this);
	}
	
	public void tick() {
		
		for (Key k : keys)
			k.tick();
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		toggle(e, true);
	}

	@Override
	public void keyReleased(KeyEvent e) {
		toggle (e, false);
	}
	
	private void toggle(KeyEvent e, boolean pressed) {
		if (e.getKeyCode() == KeyEvent.VK_UP) up.toggle(pressed); 
		if (e.getKeyCode() == KeyEvent.VK_DOWN) down.toggle(pressed);
		if (e.getKeyCode() == KeyEvent.VK_RIGHT) right.toggle(pressed);
		if (e.getKeyCode() == KeyEvent.VK_LEFT) left.toggle(pressed);
		
		if (e.getKeyCode() == KeyEvent.VK_W) up.toggle(pressed); 
		if (e.getKeyCode() == KeyEvent.VK_S) down.toggle(pressed);
		if (e.getKeyCode() == KeyEvent.VK_D) right.toggle(pressed);
		if (e.getKeyCode() == KeyEvent.VK_A) left.toggle(pressed);
		
		if (e.getKeyCode() == KeyEvent.VK_ESCAPE) escape.toggle(pressed);
		if (e.getKeyCode() == KeyEvent.VK_ENTER) enter.toggle(pressed); 
	}

}
