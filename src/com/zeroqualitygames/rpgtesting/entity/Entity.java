package com.zeroqualitygames.rpgtesting.entity;

import java.awt.Rectangle;
import java.util.ArrayList;

import com.zeroqualitygames.rpgtesting.world.World;
import com.zeroqualitygames.rpgtesting.world.tile.Tile;

public class Entity {
	public World world;
	
	public int x;
	public int y;
	public int width;
	public int height;
	
	public Rectangle rect;
	

	public Entity() {
		// TODO Auto-generated constructor stub
	}
	
	public void init(World world) {
		this.world = world;
	}
	
	public void tick() {
		rect.setLocation(x, y);
	}
	
	public void move(int vx, int vy) {
		if (vx != 0 && vy != 0) return;
		
		ArrayList<Tile> inTiles = world.getTiles(new Rectangle(rect.x + vx, rect.y + vy, rect.width, rect.height));
		for (int i = 0; i < inTiles.size(); i++) {
			if (!inTiles.get(i).canPass()) {
				return;
			}
		}
		
		x += vx;
		y += vy;
		
		rect.setLocation(x, y);
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}

}
