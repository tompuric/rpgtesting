package com.zeroqualitygames.rpgtesting.entity;

import java.awt.Rectangle;

import com.zeroqualitygames.rpgtesting.Game;
import com.zeroqualitygames.rpgtesting.InputManager;
import com.zeroqualitygames.rpgtesting.graphics.Screen;
import com.zeroqualitygames.rpgtesting.menu.TextMenu;

public class Player extends Entity {
	private Game game;
	private InputManager input;
	private int tile = 2 + 2 * 30;
	
	public String[] text = {"Hello there,", 
							"My name is", 
							"Tomislav Puric", 
							
							"Welcome :)",
							"Trolololol",
							"It's not... =D"};

	public Player(Game game, InputManager input, int x, int y) {
		this.game = game;
		this.input = input;
		this.x = x;
		this.y = y;
		width = 16;
		height = 16;
		rect = new Rectangle(x, y, width, height);
	}
	
	public void tick() {
		super.tick();
		
		if (input.enter.clicked) {
			game.setMenu(new TextMenu(text));
			//world.notifyEnter();
		}
		
		int vx = 0, vy = 0;
		
		if (input.up.pressed) vy = -1;
		if (input.down.pressed) vy = 1;
		
		move(0, vy);
		
		if (input.right.pressed) vx = 1;
		if (input.left.pressed) vx = -1;
		
		move(vx, 0);
	}
	
	public void render(Screen screen) {
		screen.render(x		, y		, tile		, 0);
		screen.render(x + 8	, y		, tile + 1	, 0);
		screen.render(x		, y + 8	, tile + 30	, 0);
		screen.render(x + 8	, y + 8	, tile + 31	, 0);
	}

}
