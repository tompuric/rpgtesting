package com.zeroqualitygames.rpgtesting.menu;

import com.zeroqualitygames.rpgtesting.graphics.Screen;

public class TextMenu extends Menu {
	private String[] text;
	private int line = 0;

	public TextMenu(String[] text) {
		super("Text");
		this.text = text;
	}
	
	public void tick() {
		
		if (input.enter.clicked) {
			line += 3;
		}
		
		if (input.escape.clicked || line >= text.length) {
			game.setMenu(null);
		}
		
	}
	
	public void render(Screen screen) {
		screen.renderTextBox(1, 14, 17, 4);
		try {
			screen.drawString(text[line], 	12, 15 * 8 - 4);
			screen.drawString(text[line+1], 12, 16 * 8	  );
			screen.drawString(text[line+2], 12, 17 * 8 + 4);
		}
		catch (Exception e) {}
	}

}
