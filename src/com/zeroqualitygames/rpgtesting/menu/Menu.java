package com.zeroqualitygames.rpgtesting.menu;

import com.zeroqualitygames.rpgtesting.Game;
import com.zeroqualitygames.rpgtesting.InputManager;
import com.zeroqualitygames.rpgtesting.graphics.Screen;

public class Menu {
	protected Game game;
	protected InputManager input;
	public String name;

	public Menu(String type) {
		name = type;
	}
	
	public void init(Game game, InputManager input) {
		this.game = game;
		this.input = input;
	}
	
	public void tick() {
		
	}
	
	public void render(Screen screen) {
		
	}

}
