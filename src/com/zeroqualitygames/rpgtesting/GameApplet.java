package com.zeroqualitygames.rpgtesting;

import java.applet.Applet;

public class GameApplet extends Applet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Game game = new Game();
	
	public GameApplet() {
		add(game);
	}
	
	public void init() {
		
	}
	
	public void stop() {
		game.stop();
	}
	
	public void start() {
		game.start();
	}

}
