package com.zeroqualitygames.rpgtesting;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

import com.zeroqualitygames.rpgtesting.entity.Player;
import com.zeroqualitygames.rpgtesting.graphics.Screen;
import com.zeroqualitygames.rpgtesting.menu.Menu;
import com.zeroqualitygames.rpgtesting.world.World;
import com.zeroqualitygames.rpgtesting.graphics.TileSet;

public class Game extends Canvas implements Runnable {

	private static final long serialVersionUID = 1L;
	public static final int WIDTH = 160;
	public static final int HEIGHT = 160;
	
	private boolean running = false;
	private Thread gameThread;
	
	private BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
	private int[] pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
	private Screen screen;
	private TileSet tileSet;
	private InputManager input = new InputManager(this);
	private Menu menu;
	private World world;
	private Player player;
	
	private final double VSYNC = 60.0;
	
	public Game() {
		setPreferredSize(new Dimension(WIDTH*3, HEIGHT*3));
		requestFocus();
		init();
	}
	
	private void init() {
		
		tileSet = new TileSet("rpgTestingTileSets2.png");
		screen = new Screen(WIDTH, HEIGHT, tileSet);	
		screen.randomise();
		world = new World(this);
		player = new Player(this, input, 50, 50);
		world.addEntity(player);
		world.init();
	}
	
	public void setMenu(Menu menu) {
		this.menu = menu;
		if (menu != null) 
			menu.init(this, input);
	}

	@Override
	public void run() {
		int sleepTime = 2;
		int tps = 0, fps = 0;
		
		long previousTime = System.nanoTime();
		double nsPerTick = 1000000000 / VSYNC;
		double unprocessed = 0;
		boolean logicInitiated = false;
		
		
		while (running) {
			long currentTime = System.nanoTime();
			long passedTime = currentTime - previousTime;
			previousTime = currentTime;
			
			unprocessed += passedTime;
			while (unprocessed > nsPerTick) {
				tick();
				tps++;
				unprocessed -= nsPerTick;
				logicInitiated = true;
				if (tps % VSYNC == 0) {
					System.out.println("tps: " + tps + "\tfps: " + fps);
					tps = 0;
					fps = 0;
				}
			}
			
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			if (logicInitiated) {
				render();
				fps++;
			}
		}
	}
	
	private void tick() {
		input.tick();
		
		if (menu == null) {
			world.tick();
		}
		else {
			menu.tick();
		}
		
	}

	private void render() {
		BufferStrategy bs = getBufferStrategy();
		if (bs == null) {
			createBufferStrategy(3);
			requestFocus();
			return;
		}
		
		paintComponent();
		
		for (int i = 0; i < pixels.length; i++) {
			/*
			int a = screen.pixels[i];
			int r = a >> 16 & 0x000000FF;
			int g = a >> 8 & 0x000000FF;
			int b = a & 0x000000FF;
			a = (r + g + b)/3;
			
			pixels[i] = a << 16 | a << 8 | a;
			*/
			pixels[i] = screen.pixels[i];
		}
		
		
		Graphics g = bs.getDrawGraphics();
		g.fillRect(0, 0, getWidth(), getHeight());
		//g.drawImage(image, (getWidth() - getHeight())/2, 0, getHeight(), getHeight(), null);
		float modifier = Math.min(getWidth()/(float)WIDTH, getHeight()/(float)HEIGHT);
		int width = (int) (WIDTH*modifier);
		int height = (int) (HEIGHT*modifier);
		
		g.drawImage(image, (getWidth() - width)/2, (getHeight() - height)/2, width, height, this);
		g.dispose();
		bs.show();
		
	}
	
	public Game getGame() {
		return this;
	}

	private void paintComponent() {
		
		world.render(screen);
		if (menu != null)
			menu.render(screen);
	}

	public void start() {
		if (!running && gameThread == null) {
			running = true;
			gameThread = new Thread(this);
			gameThread.start();
		}
	}
	
	public void stop() {
		if (running && gameThread != null) {
			running = !running;
		}
	}

}
