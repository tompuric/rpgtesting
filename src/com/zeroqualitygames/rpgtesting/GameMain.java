package com.zeroqualitygames.rpgtesting;

import java.awt.BorderLayout;

import javax.swing.JFrame;

public class GameMain extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private static final String TITLE = "RPG Testing - V0.01";
	private Game game;

	public GameMain() {
		super(TITLE);
		
		game = new Game();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(Game.WIDTH, Game.HEIGHT);
		setLocationRelativeTo(null);
		
		setName(TITLE);
		setVisible(true);
		
		setLayout(new BorderLayout());
		add(game, BorderLayout.CENTER);
		game.start();
		pack();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new GameMain();
	}

}
